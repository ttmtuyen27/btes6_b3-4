//render and show list of tasks
export let renderListOfTask = (listOfTask) => {
  let contentHTMLUncompleted = "";
  let contentHTMLCompleted = "";
  let index = 0;
  listOfTask.forEach((item) => {
    if (!item._isCompleted) {
      let contentLiUncompletedTag = /*html*/ `
            <li>
              <span> ${item.name} </span>
              <div>
                <span style="padding:3px; cursor:pointer" onclick="deleteTask(${index})"
                  ><i class="fa fa-trash-alt"></i>
                </span>
                <span style="padding:3px; cursor:pointer" onclick="changeStatus(${index})" 
                  ><i class="fa fa-check-circle"></i>
                </span>
              </div>
            </li>
          `;
      contentHTMLUncompleted += contentLiUncompletedTag;
    } else {
      let contentLiCompletedTag = /*html*/ `
            <li>
              <span> ${item.name} </span>
              <div>
                <span style="padding:3px; cursor:pointer" onclick="deleteTask(${index})"
                  ><i class="fa fa-trash-alt"></i>
                </span>
                <span style="padding:3px; cursor:pointer" onclick="changeStatus(${index})" 
                  ><i class="fa fa-check-circle"></i>
                </span>
              </div>
            </li>
          `;
      contentHTMLCompleted += contentLiCompletedTag;
    }
    index++;
  });
  document.getElementById("todo").innerHTML = contentHTMLUncompleted;
  document.getElementById("completed").innerHTML = contentHTMLCompleted;
};
