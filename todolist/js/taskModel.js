class Task {
  constructor(_name, _isCompleted) {
    this.name = _name;
    this.isCompleted = _isCompleted;
  }
}
export { Task };
