import { Task } from "./taskModel.js";
import { renderListOfTask } from "./taskController.js";

const TASK_LOCALSTORAGE = "TASK_LOCALSTORAGE";

//initialize list of tasks
let listOfTask = [];

//save data to local storage
const luuLocalStorage = () => {
  let listOfTaskJson = JSON.stringify(listOfTask);
  localStorage.setItem(TASK_LOCALSTORAGE, listOfTaskJson);
};

//render list of tasks when web start
let listOfTaskJson = localStorage.getItem(TASK_LOCALSTORAGE);
console.log(listOfTaskJson);
if (listOfTaskJson != null) {
  listOfTask = JSON.parse(listOfTaskJson);
  renderListOfTask(listOfTask);
}

//add new task and render
window.addNewTask = () => {
  let taskValue = document.getElementById("newTask").value;
  let newTask = new Task(taskValue, false);
  listOfTask.push(newTask);
  renderListOfTask(listOfTask);
  document.getElementById("newTask").value = "";
  luuLocalStorage();
};

//delete task
window.deleteTask = (index) => {
  listOfTask.splice(index, 1);
  renderListOfTask(listOfTask);
  luuLocalStorage();
};

//change task from uncompleted to completed
window.changeStatus = (index) => {
  listOfTask[index]._isCompleted = true;
  renderListOfTask(listOfTask);
  luuLocalStorage();
};

//sort list of task in alphabetical order and in reverse
window.sortAtoZ = (bool) => {
  if (bool) {
    listOfTask.sort((a, b) => a.name.localeCompare(b.name));
  } else {
    listOfTask.sort((a, b) => -1 * a.name.localeCompare(b.name));
  }
  renderListOfTask(listOfTask);
  luuLocalStorage();
};
